package com.arcada.devops;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Test;

public class PositiveAppTest {

	// Positive testing
	// Testing different ways to make tests. There is similarities 
	@Test
	public final void testMain() {
		String[] names = App.fetchFriends();
		
		boolean foundName = false;
		for (String name: names) {
			
		  	if("marko".equals(name)) 
		  	{
		  		foundName = true;
	            break;
    		
		  	}
		}
	    assertTrue(foundName);
	    assertFalse(!foundName);
	}
	@Test
    public void test() {
        String[] expNames = {"pelle", "jarkko", "marko"};
        String[] names = App.fetchFriends();
        Assert.assertArrayEquals(expNames, names);
    }
	@Test
    public void testLength() {
        String[] names = App.fetchFriends();
        Assert.assertEquals(3, names.length);
    }

}
