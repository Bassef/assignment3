package com.arcada.devops;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{	
	// Negative testing
    // Testing different ways to make tests. There is similarities 
    @Test
    public void shouldAnswerWithTrue()
    {
    	String[] names = App.fetchFriends();
    	
    	if(names == null) 
    	{
    		assertTrue( false );
    		
    	}
    	
    	else if(names.length < 1) 
    	{
    		assertTrue( false );
    	}
    	else 
    	{
    		assertTrue( true );
    	}
    	
    }
    @Test
    public void testEmpty() {
        String[] names = App.fetchFriends();
        Assert.assertTrue(names.length > 0);
    }
    
    @Test
    public void testFirst() {
        String[] names = App.fetchFriends();
        Assert.assertNotEquals("john", names[0]);
    }
}
