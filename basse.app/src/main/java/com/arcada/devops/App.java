package com.arcada.devops;


public final class App 
{
	// Skeletal code for fetchFriends()
	public static String[] fetchFriends()
	{
		// TODO: Implement the actual method logic here
		String[] names = {"pelle", "jarkko", "marko"};
		
		return names;
	}
	 

    public static final void main(final  String[] args )
    {
    	String[] names = fetchFriends();
    	
    	for (String name: names) {
    		System.out.println(name);
    	}
    	
    }
}
